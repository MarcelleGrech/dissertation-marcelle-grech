-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 17, 2020 at 10:14 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id12621514_prototype`
--

-- --------------------------------------------------------

--
-- Table structure for table `playerinfo`
--

CREATE TABLE `playerinfo` (
  `playerid` int(11) NOT NULL,
  `playername` varchar(255) NOT NULL,
  `playeremail` varchar(255) NOT NULL,
  `score` varchar(255) NOT NULL,
  `random` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playerinfo`
--

INSERT INTO `playerinfo` (`playerid`, `playername`, `playeremail`, `score`, `random`) VALUES
(1, '$playername', '$playeremail', '1231', ''),
(2, 'test', 'testing@gmail.com', '123', ''),
(3, 'playername', 'playeremail', '$score', ''),
(4, 'playername', 'playeremail', '$score', ''),
(5, '', '', '12', ''),
(6, '', '', '12', ''),
(7, '', '', '12', ''),
(8, '', '', '12', ''),
(9, '', '', '12', ''),
(10, '', '', '12', ''),
(11, 'aerw', 'werwee', '', ''),
(12, 'emily', '123', '', ''),
(13, 'trerer', 'eteter', '', ''),
(14, 'sewt', 'sfgg', '', ''),
(15, 'rwrt', 'rrrrr', '', ''),
(16, 'retet', 'wrterer', '', ''),
(17, 'playername', 'playeremail', '$score', ''),
(18, 'dsgrertert', 'retertertrterte', '', ''),
(19, 'teating', 'marisol@gmail.com', '', ''),
(20, 'playername', 'playeremail', 'score', 'random'),
(21, 'drterata', 'aeryaeaeete', '33', ''),
(22, 'dxgxd', 'tstye', '', '447'),
(23, 'marisol', 'marcellechristinegrech96@gmail.com', '', '547'),
(24, 'marceline', 'jsrirehjererer', '', '650'),
(25, 'stystyetyety', 'stustytytytyrtyrtysrtsrt', '', '1004'),
(26, 'srtertser', 'ertsertesrte', '12321', '798'),
(27, 'rtetereter', 'rtrtrrrrrrrrrrrrrrrrrrrrrr', '', '660'),
(28, 'marisol', 'marisoleee', '', '471'),
(29, 'rtyrtyertyry', 'rytyrtrrtrttttr', '', '371'),
(30, 'ertert', 'erterter', '', '788'),
(31, 'drtytrdtyr', 'tytyrtyryrtry', '', '34'),
(32, 'zdrgdrgzszrt', 'dgtrtreertrererr', '', '10'),
(33, 'cfhdtyet', 'tyetyrtyrtyrttyr', '', '823'),
(34, 'justsaying', 'plswork', '', '816'),
(35, 'plswork', 'plswork', '', '500'),
(36, 'xdrtgseyseyset', 'sdyryeeyey', '', '709');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `playerinfo`
--
ALTER TABLE `playerinfo`
  ADD PRIMARY KEY (`playerid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playerinfo`
--
ALTER TABLE `playerinfo`
  MODIFY `playerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
