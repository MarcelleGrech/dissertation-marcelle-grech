<!Doctype html>
<html>
<head>
<title>Marcelle Grech </title>  
    <link href="style.css" rel="stylesheet" type="text/css">
    <link href="bootstrap/bootstrap-4.4.1-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <script src="bootstrap/bootstrap-4.4.1-dist/js/bootstrap.min.js"></script>
    <script src="bootstrap/bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
</head>

    <body>
<div class="jumbotron">
  <h1 class="display-4">Pistol Target Shooting VR Game</h1>
  <p class="lead">Studying the Effects of VirtualReality as a Tool for Learning or Improving a new skill in Sport Education, Specifically for Target Practice Shooting with Pistols.</p>
  <hr class="my-4">
  <p>This is the landing page for my dissertation prototype.</p>
    
    <br/>
    <br/>
    <div class="row" style="text-align:center">
    <div class="col-lg-4">
        <div class="card">
          <img src="img/survey1.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Surveys</h5>
            <p class="card-text">This section contains information regarding the surveys that were conducted in this study.</p>
            <a href="#" class="btn btn-primary">Learn more</a>
          </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <br/>
            <br/>
          <img src="img/research1.png" class="card-img-top" alt="..." style="margin-bottom:85px;">
          <div class="card-body" >
            <h5 class="card-title">Research</h5>
            <p class="card-text">This section contains information regarding the research that was conducted in this study.</p>
            <a href="#" class="btn btn-primary">Learn more</a>
          </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
          <img src="img/survey.png" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Results</h5>
            <p class="card-text">This section contains the results that were gathered in this study.</p>
            <a href="#" class="btn btn-primary">Learn more</a>
          </div>
        </div>
    </div>
    </div>
    
    <br/>
    <br/>
    <div class="float-right">
        <h5> Marcelle Christine Grech</h5>
        <h6 style="float:right;">May 2020</h6>
    </div>
</div>

    </body>
</html>

























<?php
error_reporting(0);

//error_reporting(0);
//the first parameter remains localhost
//second parameter is db username
//third is db password
//fourth is db name
//fifth is db port, default port is 3306

$db = new mysqli('localhost','id12621514_prototypedb','password','id12621514_prototype',3306);
$dboperation = mysqli_real_escape_string($db,$_GET['operation']);
$dboperation = strtoupper($dboperation);

if($dboperation =="POST")
{
	$playername = mysqli_real_escape_string($db,$_GET['playername']);
	$playeremail = mysqli_real_escape_string($db,$_GET['playeremail']);
	$score = mysqli_real_escape_string($db,$_GET['score']);
    $random = mysqli_real_escape_string($db,$_GET['random']);

	$query = "INSERT INTO playerinfo(playername,playeremail,score,random) VALUES ('$playername','$playeremail','$score','$random');";

	if($db->query($query))
	{
		echo "OK|".$db->insert_id;
	}
	else
	{
		echo $query;
		echo $db->error();
	}
}

if ($dboperation == "HIGHSCORE")
{
	$random = mysqli_real_escape_string($db,$_GET['random']);
	$name   = mysqli_real_escape_string($db,$_GET['playername']);
	$score = mysqli_real_escape_string($db,$_GET['score']);

	//if mysql error
	if (mysqli_connect_errno()){
		echo 'error connecting';
		exit;
		
	}
	
	$sql = "update playerinfo set score = '$score' where random='$random' AND playername='$name'";
	
	
	if ($db->query($sql))
	{
		echo "OK";
	}
	else {
		echo $sql;
		echo $db->error;
	}
}

?>