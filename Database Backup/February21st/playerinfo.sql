-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 21, 2020 at 02:18 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `id12621514_prototype`
--

-- --------------------------------------------------------

--
-- Table structure for table `playerinfo`
--

CREATE TABLE `playerinfo` (
  `playerid` int(11) NOT NULL,
  `playername` varchar(255) NOT NULL,
  `playeremail` varchar(255) NOT NULL,
  `score` varchar(255) NOT NULL,
  `random` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `playerinfo`
--

INSERT INTO `playerinfo` (`playerid`, `playername`, `playeremail`, `score`, `random`) VALUES
(1, '$playername', '$playeremail', '1231', ''),
(2, 'test', 'testing@gmail.com', '123', ''),
(3, 'playername', 'playeremail', '$score', ''),
(4, 'playername', 'playeremail', '$score', ''),
(5, '', '', '12', ''),
(6, '', '', '12', ''),
(7, '', '', '12', ''),
(8, '', '', '12', ''),
(9, '', '', '12', ''),
(10, '', '', '12', ''),
(11, 'aerw', 'werwee', '', ''),
(12, 'emily', '123', '', ''),
(13, 'trerer', 'eteter', '', ''),
(14, 'sewt', 'sfgg', '', ''),
(15, 'rwrt', 'rrrrr', '', ''),
(16, 'retet', 'wrterer', '', ''),
(17, 'playername', 'playeremail', '$score', ''),
(18, 'dsgrertert', 'retertertrterte', '', ''),
(19, 'teating', 'marisol@gmail.com', '', ''),
(20, 'playername', 'playeremail', 'score', 'random'),
(21, 'drterata', 'aeryaeaeete', '33', ''),
(22, 'dxgxd', 'tstye', '', '447'),
(23, 'marisol', 'marcellechristinegrech96@gmail.com', '', '547'),
(24, 'marceline', 'jsrirehjererer', '', '650'),
(25, 'stystyetyety', 'stustytytytyrtyrtysrtsrt', '', '1004'),
(26, 'srtertser', 'ertsertesrte', '12321', '798'),
(27, 'rtetereter', 'rtrtrrrrrrrrrrrrrrrrrrrrrr', '', '660'),
(28, 'marisol', 'marisoleee', '', '471'),
(29, 'rtyrtyertyry', 'rytyrtrrtrttttr', '', '371'),
(30, 'ertert', 'erterter', '', '788'),
(31, 'drtytrdtyr', 'tytyrtyryrtry', '', '34'),
(32, 'zdrgdrgzszrt', 'dgtrtreertrererr', '', '10'),
(33, 'cfhdtyet', 'tyetyrtyrtyrttyr', '', '823'),
(34, 'justsaying', 'plswork', '', '816'),
(35, 'plswork', 'plswork', '', '500'),
(36, 'xdrtgseyseyset', 'sdyryeeyey', '', '709'),
(37, 'xdthydydrtydrty', 'rtyrtrttrrtttrttt', '', '304'),
(38, 'marisol', 'marisol', '', '227'),
(39, 'yoyoyoyoy', 'yoyoyoyoyoy', '', '412'),
(40, 'dtryrtyyyttyryrt', 'yrtyrttrttrtttt', '', '438'),
(41, 'dghdhd', 'tytyryrtyrtyr', '', '613'),
(42, 'ff', 'ff', '', ''),
(43, 'dtyeyety', 'tytrrtyrttr', '', '649'),
(44, 'a', 'a', '', '752'),
(45, 'asda', 'sfs', '', '660'),
(46, 'dfsf', 'dfsf', '', '467'),
(47, 'srrtrert', 'ertere', '', '773'),
(48, 'erteer', 'reerre', '', ''),
(49, 'dgdfgdf', 'dfdfdfd', '', '800'),
(50, 'gfdfggdf', 'dfdgfff', '', '430'),
(51, 'tttttttttttttt', 'tttttttttttttttttt', '', '263'),
(52, 'gggggggggggggggg', 'gggggggggggggggggg', '', '955'),
(53, 'ddrer', 'erteretret', '', '381'),
(54, 'sdrtrtetr', 'eterte', '', '234'),
(55, 'ftyr', 'rtyrtr', '12', '216'),
(56, 'hxnzmz', 'nxjzna', '', '478'),
(57, 'hxhsns', 'bxnJ', '', '827'),
(58, 'hzbzj', 'nzbz', '', '469'),
(59, 'hzjzb', 'hzjBa', '', '117'),
(60, 'nzhV', 'bNJaja', '', '747'),
(61, 'yghhh', 'yhhg', '', '25'),
(62, 'gtg', 'ffyt', '', '852'),
(63, 'hghj', 'fjv', '', '541'),
(64, 'dylsn', 'dylan.delxepp', '', '495'),
(65, 'hjiihn', 'marc', '', '902'),
(66, 'fhfh', 'vfhh', '', '386'),
(67, 'safdwefwafwrew', 'eeeeeeeeeeeeeeeeeee', '', '763'),
(68, 'finaltestingpls', 'finaltestingplease', '180', '850');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `playerinfo`
--
ALTER TABLE `playerinfo`
  ADD PRIMARY KEY (`playerid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `playerinfo`
--
ALTER TABLE `playerinfo`
  MODIFY `playerid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
