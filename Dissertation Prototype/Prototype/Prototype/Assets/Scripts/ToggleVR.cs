﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class ToggleVR : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(VRActivator("cardboard"));
    }
    IEnumerator VRActivator(string device)
    {
        XRSettings.LoadDeviceByName(device);
        yield return null;
        XRSettings.enabled = true;
    }

}
