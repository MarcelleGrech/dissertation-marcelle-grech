﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public InputField playerNameInput, playerEmailInput;
    string playerEmail, playerName, score;
    GameObject datastore;
    public Text alert;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReadyBtn()
    {
        if(playerNameInput.text == "" || playerEmailInput.text == "") 
        //if one of these fields is blank set text to 'Please enter valid data'
        {
            alert.text ="please enter valid data.";
        }
        else
        {
            //referencing the gameobject with name 'Datastore' on unity screen 
            datastore = GameObject.Find("Datastore");
            //setting playerName from the 'Datastore' to be the same as 'playerName' from this script
            datastore.GetComponent<Datastore>().playerName = playerName;
            //setting playerEmail from the 'Datastore' to be the same as 'playerEmail' from this script
            datastore.GetComponent<Datastore>().playerEmail = playerEmail;
            //loading the loaderScreen 
            SceneManager.LoadScene("LoaderScreen");
            //after the code above, startcoroutine to enter data in the database.
            StartCoroutine(Camera.main.GetComponent<Connection>().joinGame()); 

        }
    }
}
