﻿using UnityEngine;
using System.Collections;
public class ClayPidgeon : MonoBehaviour
{
    public GameObject ClayPigeonPrefab;
    public AudioSource audioSource;
    public float  minimumHorizontalForce;
    public float minimumVerticalForce;
    public float maximumHorizontalForce;
    public float maximumVerticalForce;
    public float health = 50f;
    public int targetCounterforClay;
    int throwDirection;


 //Use this for initialization
 void Start()
 {
        throwDirection =1;

    if(transform.position.x > 0)
        {
            throwDirection = -1;
        }

    GetComponent<Rigidbody>().AddForce( new Vector3(
    throwDirection * Random.Range(minimumHorizontalForce, maximumHorizontalForce),
    Random.Range(minimumVerticalForce, maximumVerticalForce),0)); 
 }
 
 //Update is called once per frame
 void Update()
 {
        
 }
 
 void OnTriggerEnter(Collider otherCollider)
 {
     if (otherCollider.gameObject.tag =="Terrain")// if objct hits floor
     {
         //destroy gameobject here
         Destroy(gameObject);
        //gameObject.SetActive(false);
        //add to the amount of total targets in environment;
     }
 }
 
// IEnumerator GenerateClayPidgeon()
//  {
//     yield return new WaitForSeconds(5f);
//     Instantiate(ClayPigeonPrefab, new Vector3((float)x,(float)y,(float)z), Quaternion.identity);
//     StartCoroutine(ThrowClayPidgeon());
    
//  }

    IEnumerator ThrowClayPidgeon()
    {
        throwDirection =1;
        if(transform.position.x > 0)   
        {
            throwDirection = -1;
            }
        GetComponent<Rigidbody>().AddForce( new Vector3(
        throwDirection * Random.Range(minimumHorizontalForce, maximumHorizontalForce),
        Random.Range(minimumVerticalForce, maximumVerticalForce),0));
        
        yield return new WaitForSeconds(0);
    }

  public void TakeDamage(float amount)
{
    health -= amount;
    if(health <= 0f)
    {
        //Destroy(gameObject,0f);
        //explosion.Play();
        audioSource.Play();
        Destroyed();
    }
}

    void Destroyed()
    {
        Destroy(gameObject);
        GameController gc = Camera.main.GetComponent<GameController>();
        gc.ClayPidgeonDownMethod();
    }
}
