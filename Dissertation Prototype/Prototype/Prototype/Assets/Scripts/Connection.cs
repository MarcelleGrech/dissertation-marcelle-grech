﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System.Net.NetworkInformation; 
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Connection : MonoBehaviour
{
    #region Variable Declarations
    static string playername ="", playeremail="", score="", random="";
    int randomNumber;
    //string url = "http://localhost/";
    string url = "https://virtualrealityprototype.000webhostapp.com/"; // this link is going to be used in the real life  testing of the game
    // Start is called before the first frame update

    #endregion

    void Start(){}

void OnSceneLoaded(Scene scene, LoadSceneMode mode)
{  
 
    playername = GameObject.Find("Datastore").GetComponent<Datastore>().playerName;
    playeremail = GameObject.Find("Datastore").GetComponent<Datastore>().playerEmail;
    score = GameObject.Find("Datastore").GetComponent<Datastore>().score;
    random = GameObject.Find("Datastore").GetComponent<Datastore>().random;
}

 public IEnumerator joinGame()
 {
    GenerateRandom(); //generates radom number for 'random'
    playername = GameObject.Find("PlayerNameInput").GetComponent<InputField>().text;
    playeremail = GameObject.Find("PlayerEmailInput").GetComponent<InputField>().text;
    string joinUrl = url + "?operation=post&playername="+playername+"&playeremail="+playeremail+"&score="+score+"&random="+random;

    Debug.Log(joinUrl);

    UnityWebRequest www = UnityWebRequest.Get(joinUrl);

    yield return www.SendWebRequest();

    if(www.isHttpError || www.isNetworkError)
    {
        Debug.Log(www.error);
    }
    else
    {
       // Debug.Log("DONE");
    }
 }


#region Updating Score into the Database Code
 public void UpdateScoreInDatabase()
 {

     StartCoroutine(UpdateScore());
 }

    public IEnumerator UpdateScore()
    {

        //GameController gamecontroller = Camera.main.GetComponent<GameController>();
        int scoreInfo = GameController.scoreCounter;
        int totalTargets = GameController.targetCounter;
            //Datastore d = Camera.main.GetComponent<Datastore>();
            string scoreURL = url + "?operation=highscore&random=" + random + "&playername=" + playername + "&score=" + "hits: "+scoreInfo + " total targets: "+totalTargets;
            UnityWebRequest www = UnityWebRequest.Get(scoreURL);
            Debug.Log(scoreURL);
            yield return www.SendWebRequest();
            yield return new WaitForSeconds(120f);
        
    }

#endregion
//generates random number between 0 and 1024
public void GenerateRandom()
{
    randomNumber = Random.Range(0,1024);
    random = randomNumber.ToString();
}
}
