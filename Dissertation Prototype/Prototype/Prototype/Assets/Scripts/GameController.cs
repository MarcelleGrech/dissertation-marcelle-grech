﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System.Net.NetworkInformation; 
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameController : MonoBehaviour
{

public AudioSource BackgroundMusic;
public GameObject target_One,target_Two, target_Three, ClayTarget;
public static int scoreCounter, targetCounter = 3; 
public Text timerTxt;
public GameObject timergo;
float timer = 120.0f; //two minutes
float timeToRespawn = 0;
float timeToUpdateDatabase = 0;
string url = "https://virtualrealityprototype.000webhostapp.com/";

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update() // Contains (Respawning Clay Pidgeons, Timer and Printing of Score)
    {

        #region Respawn ClayPidgeons
        timeToRespawn += Time.deltaTime;

        if( timeToRespawn >= 2f)
        {
            SpawnTarget();
            timeToRespawn =0;
        }
        #endregion  
      
        #region Timer 
        timer -= Time.deltaTime;
        float minutes = Mathf.Floor(timer / 60);
        float seconds = timer%60;
        timerTxt.text = "Timer:  " + minutes + ":" + Mathf.RoundToInt(seconds);

        //Time is up
        if(timer <= 0)
        {
            timergo.SetActive(false);
            timeToUpdateDatabase +=1;
            if(timeToUpdateDatabase >= 120f)
            {
            //timerTxt.enabled = false;
           // Debug.Log("TIME IS UP");
            BackgroundMusic.Stop();
            UpdateDatabase();
            timeToUpdateDatabase=0;
            SceneManager.LoadScene("End");

            }
            
        

        }
        #endregion
       
        #region Printing The Number Of Overall Targets and Score
       if(Input.GetKeyDown(KeyCode.K))
       {
           Debug.Log("Number of Targets So FAR : " + targetCounter + "\n SCORE NUMBER: " + scoreCounter);
       }
        #endregion


    }


void UpdateDatabase()
{

        Connection c = Camera.main.GetComponent<Connection>();
        c.UpdateScoreInDatabase();
}
#region  Target Type: Bottles (Target One , Target Two & Target Three)
    public void TargetOne_REVIVED()
    {
        StartCoroutine(TargetOne_Respawn());
    }
   IEnumerator TargetOne_Respawn()
    {
        target_One.SetActive(false);
        scoreCounter+=1;
        yield return new WaitForSeconds(5f);
       // Debug.Log("WE ARE HERE");
        target_One.SetActive(true);
        targetCounter+=1;
    }
    
    public void TargetTwo_REVIVED()
    {
        StartCoroutine(TargetTwo_Respawn());
    }

    IEnumerator TargetTwo_Respawn()
    {
        target_Two.SetActive(false);
        scoreCounter +=1;
        yield return new WaitForSeconds(5f);
       // Debug.Log("WE ARE HERE");
        target_Two.SetActive(true);
        targetCounter+=1;
    }


    public void TargetThree_REVIVED()
    {
        StartCoroutine(TargetThree_Respawn());
    }

    IEnumerator TargetThree_Respawn()
    {
        target_Three.SetActive(false);
        scoreCounter+=1;
        yield return new WaitForSeconds(5f);
       // Debug.Log("WE ARE HERE");
        target_Three.SetActive(true);
        targetCounter+=1;
    }
#endregion
   
#region Target Type :  Clay Pidgeon
public void ClayPidgeonDownMethod()
{
    StartCoroutine(ClayPidgeonDown());
}
IEnumerator ClayPidgeonDown()
{
    yield return new WaitForSeconds(0f);
    scoreCounter+=1;
}

void SpawnTarget()
{
    GameObject clayPidgeonTarget = Instantiate(ClayTarget);
    clayPidgeonTarget.transform.position = new Vector3(-20,5, 50); 
    targetCounter+=1;
}
#endregion

        # region  Commented - UpdateScore
        // IEnumerator UpdateScore()
        // {
        //     while (true)
        //     {
        //         Connection c= Camera.main.GetComponent<Connection>();
        //      string scoreURL =
        //         url + "?operation=highscore&random=" +c.random + "&playername=" + c.playername + "&score=" + "Hits: " +scoreCounter+"Over Total: " +targetCounter;
        //         UnityWebRequest www = UnityWebRequest.Get(scoreURL);
        //         Debug.Log(scoreURL);
        //         yield return www.SendWebRequest();
        //     }
        // }
        
        // public  void UPDATETHESCORENOW()
        // {
        //     score = "12";
        // }
        
        // public void ONCLICK()
        // {
        // UPDATETHESCORENOW();
        // StartCoroutine(UpdateScore());
        // }
 #endregion
}
