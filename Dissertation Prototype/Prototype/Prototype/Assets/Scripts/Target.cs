﻿using UnityEngine;

public class Target : MonoBehaviour
{
public float health = 50f;
public void TakeDamage(float amount)
{
    health -= amount;
    if(health <= 0f)
    {
        //Destroy(gameObject,0f);
        Destroyed();
    }
}
    void Destroyed()
    {
        GameController gc = Camera.main.GetComponent<GameController>();
        if(gameObject.tag =="Target-1")
        {
            //Debug.Log("ENTERED");
            gc.TargetOne_REVIVED();
           // StartCoroutine(gc.TargetOne_Respawn());
        }
        else if(gameObject.tag == "Target-2")
        {
               // Debug.Log("ENTERED TARGET 2");
                gc.TargetTwo_REVIVED();
        }
        else if(gameObject.tag == "Target-3")
        {
                //Debug.Log("ENTERED TARGET 3");
                gc.TargetThree_REVIVED();
        }
        //Destroy(gameObject,0f);

    }
}

