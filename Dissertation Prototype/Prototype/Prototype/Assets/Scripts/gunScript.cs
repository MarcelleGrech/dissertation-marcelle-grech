﻿using UnityEngine;

public class gunScript : MonoBehaviour
{
    public float damage = 100f;
    public float range = 200f;
    public float fireRate = 15f;
    private float nextTimeToFire = 0f;
    public Camera fpsCam;
    public ParticleSystem muzzleFlash;
    public AudioSource audioSource;
    public AudioClip ClayTargetSound, BottleTargetSound;
    

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1") && Time.time >=nextTimeToFire)
        {
            nextTimeToFire = Time.time + 1f/fireRate; //intervals of 2.5seconds
            Shoot();
        }
    }

    void Shoot()
    {
        audioSource.Play();
        muzzleFlash.Play();
        RaycastHit hit;
        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();
            ClayPidgeon CP = hit.transform.GetComponent<ClayPidgeon>();

            if(target != null)
            {
                audioSource.PlayOneShot(BottleTargetSound);
                target.TakeDamage(damage);
            }

            if(hit.transform.name == "ClayPidgeon1(Clone)")
            {
                //Debug.Log("YOOOOOOOOOOOOO");
                if(CP != null)
                {
                    audioSource.PlayOneShot(BottleTargetSound);
                    audioSource.PlayOneShot(ClayTargetSound);
                    CP.TakeDamage(damage);
                }
            }
        }
    }
}
