﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
public class NOVR : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(VRDeactivator("none"));
    }

IEnumerator VRDeactivator(string device)
{
        XRSettings.LoadDeviceByName(device);
        yield return null;
        XRSettings.enabled = false;
}
}
