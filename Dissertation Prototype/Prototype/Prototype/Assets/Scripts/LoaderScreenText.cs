﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoaderScreenText : MonoBehaviour
{
    public GameObject  txtLabel, txtLabel2;
    private float nextActionTime = 0.0f;
    public float period = 2f;
    bool change =false;


    // Start is called before the first frame update
    void Start()
    {    }

    // Update is called once per frame
    void Update()
    {

        if(Input.GetButtonDown("Fire1"))
        {
            SceneManager.LoadScene("GameEnvironment");
        }
        if (Time.time > nextActionTime) 
        {
            nextActionTime += period;
            ToggleText();
        }
    }

    void ToggleText() // this method toggles text from enabled to disabled
    {
        if(change == false)
        {
            txtLabel.SetActive(false);
            txtLabel2.SetActive(true);
            change = true;
        }
        else
        {
            txtLabel.SetActive(true);
            txtLabel2.SetActive(false);
            change = false;
        }
    }
}
